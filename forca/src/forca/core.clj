(ns forca.core
  (:gen-class))

(def totalDeVidas 6)
(def palavraSecreta "MELANCIA")

(defn perdeu[] (print "Você perdeu!"))
(defn ganhou[] (print "Você Ganhou!"))

(defn letrasFaltantes [palavra acertos]
	(remove (fn [letra]	(contains? acertos (str letra))) palavra))

(defn acertouAPalavraToda? [palavra acertos]
	(empty? (letrasFaltantes palavra acertos)))

(defn acertou? [chute palavra]
	(.contains palavra chute))



(defn leLetra! [] (read-line))

(defn imprimeForca[vidas palavra acertos]
	(println "Vidas" vidas)
	(doseq [letra (seq palavra)]
		(if (contains? acertos (str letra))
			(print letra " ") (print "_" " ")))
	(println))

(defn jogo[vidas palavra acertos]
	(imprimeForca vidas palavra acertos)
	(cond
		(= vidas 0) (perdeu)
		(acertouAPalavraToda? palavra acertos) (ganhou)
	:else
		(let [chute (leLetra!)]
		 	(if (acertou? chute palavra)
		 		(do
		 			(println "Acertou a letra!")
					(recur vidas palavra (conj acertos chute))
		 		)
		 		(do
		 			(println "Errou a letra, perdeu uma vida!")
					(recur (dec vidas) palavra acertos))))))


(defn avaliaChute[chute vidas palavra acertos]
	(if (acertou? chute palavra)
		(jogo vidas palavra (conj acertos chute))
		(jogo (dec vidas) palavra acertos)))


(def listaNumeros #{1 2 3 4 5})
(def carros #{50000.0 60000.0})




(defn soma[n]
	(loop [contador 1 soma 0]
		(if(> contador n) soma
			(recur (inc contador) (+ soma contador)))))

(defn fib[x]
    (loop [a 1 b 1 numero 2]
    	(if(= numero x) b
    		(recur b (+ a b) (inc numero)))))


(defn comecaJogo[]
	(jogo totalDeVidas palavraSecreta #{})
)

(defn -main [& args]
  (comecaJogo))
